# -*- coding: utf-8 -*-
from argparse import ArgumentParser
from urllib.parse import quote as escape
import json
from jinja2 import Environment, FileSystemLoader
import os
from shutil import rmtree
import webbrowser
from time import time


arg_parser = ArgumentParser(description="Take wikipedia pages in XML format "
                            + " and generate HTML pages containing articles, "
                            + "which are mentioned between \"MIN_ENTRIES\" "
                            + "and \"MAX_ENTRIES\" params times")
arg_parser.add_argument("--file", "-f", type=str, metavar="WIKI_FILE",
                        default="ruwiki-latest-pages-articles-multistream.xml",
                        help="directory + wikipedia dump file name (default:" +
                        " ruwiki-latest-pages-articles-multistream.xml)")
arg_parser.add_argument("--eta_freq", "--eta", type=int, metavar="ETA_FREQ",
                        default=1000, help="one ETA print per ETA_FREQ " +
                        "articles. Note that ETA assumes 1.5M articles to be" +
                        " proceeded")
arg_parser.add_argument("--min", type=int, default=2500, metavar="MIN_ENTRIES",
                        help="MIN number of entries for the article "
                        + "to be included (default: 2500)")
arg_parser.add_argument("--max", type=int, default=5000, metavar="MAX_ENTRIES",
                        help="MAX number of entries for the article "
                        + "to be included (default: 5000)")
arg_parser.add_argument("--htmldir", "--htm", type=str, default="html",
                        metavar="HTML_OUTPUT", help="directory of html pages")
arg_parser.add_argument("--lim", type=int, default=1000, metavar="PAGE_LIMIT",
                        help="limit of links on one HTML page (default: 1000)")
arg_parser.add_argument("--backup_freq", "--freq", type=int, default=1000,
                        metavar="BACKUP_FREQ", help="1 backup of link_" +
                        "entries per BACKUP_FREQ articles (default: 1000)")
arg_parser.add_argument("--backup_file", type=str, default="backup.json",
                        metavar="BACKUP_FILE", help="directory + backup file" +
                        " name (default: backup.json")
arg_parser.add_argument("--html_template", "--html_temp", "--temp", type=str,
                        metavar="HTML_TEMPLATE", default="html_template.html",
                        help="path to HTML template: Jinja2 format " +
                        "(default: html_template.html)")
arg_parser.add_argument("--dev", action="store_true",
                        help="enable dev logging. NOT to be used on big files")
arg_parser.add_argument("--no_backup", "--noback", action="store_true",
                        help="disable link_entries dictionary backup. " +
                        "If passed, other backup params will be ignored.")
arg_parser.add_argument("--resume", "-r", action="store_true",
                        help="resume work using backup at BACKUP_FILE. " +
                        "Could not be activated if --noback flag passed. " +
                        "Also, that flag could be used to differ output " +
                        "params after processing a file once.")


def unpack_args():
    global arg_parser, WIKI_FILE, ETA_FREQ, MIN_ENTRIES, MAX_ENTRIES
    global HTML_OUTPUT, PAGE_LIMIT, DEV, BACKUP_FREQ, BACKUP_FILE
    global HTML_TEMPLATE, NO_BACKUP, RESUME
    ns = arg_parser.parse_args()
    WIKI_FILE = ns.file
    ETA_FREQ = ns.eta_freq
    MIN_ENTRIES = ns.min
    MAX_ENTRIES = ns.max
    HTML_OUTPUT = ns.htmldir
    PAGE_LIMIT = ns.lim
    DEV = ns.dev
    BACKUP_FREQ = ns.backup_freq
    BACKUP_FILE = ns.backup_file
    HTML_TEMPLATE = ns.html_template
    NO_BACKUP = ns.no_backup
    RESUME = not ns.no_backup and ns.resume


# some specific states have their own names
SEEK_LINK = 1
READ_LINK = 3
PROCESS_LINK = 5
NOWIKI = 13

SWITCH_MATRIX = {
    SEEK_LINK: {"[": 2, "<": 6},
    2: {"[": READ_LINK, "ANY": SEEK_LINK},
    READ_LINK: {"]": PROCESS_LINK,
                "#": PROCESS_LINK,
                "|": PROCESS_LINK,
                "/": PROCESS_LINK,
                "{": 4},
    4: {"{": SEEK_LINK, "ANY": READ_LINK},
    PROCESS_LINK: {"ANY": SEEK_LINK},
    6: {"n": 7, "ANY": SEEK_LINK},
    7: {"o": 8, "ANY": SEEK_LINK},
    8: {"w": 9, "ANY": SEEK_LINK},
    9: {"i": 10, "ANY": SEEK_LINK},
    10: {"k": 11, "ANY": SEEK_LINK},
    11: {"i": 12, "ANY": SEEK_LINK},
    12: {">": NOWIKI, "ANY": SEEK_LINK},
    NOWIKI: {"<": 14},
    14: {"/": 15, "ANY": NOWIKI},
    15: {"n": 16, "ANY": NOWIKI},
    16: {"o": 17, "ANY": NOWIKI},
    17: {"w": 18, "ANY": NOWIKI},
    18: {"i": 19, "ANY": NOWIKI},
    19: {"k": 20, "ANY": NOWIKI},
    20: {"i": 21, "ANY": NOWIKI},
    21: {">": SEEK_LINK, "ANY": NOWIKI}
}

SPECIAL_STATES = {
    READ_LINK: 1, 4: 1,
    PROCESS_LINK: 2
}

state = SEEK_LINK

link_arr = []


def record_link(sym):
    global link_arr, state
    link_arr.append(sym)
    state = get_new_state(state, sym)


def startswith(s, prefixes):
    for prefix in prefixes:
        if s.lower().startswith(prefix):
            return True
    return False


link_entries = dict()  # key: link; value: number of entries


def process_link(sym):
    global link_arr, link_entries, state
    link = "".join(link_arr[:-1])
    if (not startswith(link, ["category", "категория"])) and link:
        link = link.strip(":[]#|/{}")
        if not startswith(
            link, ["special:", "image:", "file:", "media:", "mediawiki:",
                   "служебная:", "изображение:", "файл:", "медиа:"]
        ):
            if link.startswith("ВП"):
                link = "".join(["Википедия", link[2:]])
            if len(link) != 0:
                link = "".join([link[0].upper(), link[1:]])
                link_entries[link] = link_entries.get(link, 0) + 1
    link_arr = []
    state = get_new_state(state, sym)


HANDLERS = {
    1: record_link,
    2: process_link
}


def get_new_state(state, sym):
    global SWITCH_MATRIX
    if sym in SWITCH_MATRIX[state]:
        return SWITCH_MATRIX[state][sym]
    elif ("ANY" in SWITCH_MATRIX[state] and
            SWITCH_MATRIX[state]["ANY"] != state):
        # second condition should never be true, but who knows...
        return get_new_state(SWITCH_MATRIX[state]["ANY"], sym)
    else:
        return state


def special_run(sym):
    global HANDLERS, SPECIAL_STATES
    HANDLERS[SPECIAL_STATES[state]](sym)


def process_symbol(sym):
    global state, SPECIAL_STATES
    if state in SPECIAL_STATES:
        special_run(sym)
    else:
        state = get_new_state(state, sym)


def backup(proceeded, stop_byte):
    global link_entries, BACKUP_FILE
    with open(BACKUP_FILE, "w", encoding="utf8") as backup_file:
        json.dump([proceeded, stop_byte, link_entries], backup_file,
                  ensure_ascii=False, separators=(",", ":"))


def unpack_backup():
    global link_entries, BACKUP_FILE
    with open(BACKUP_FILE, "r", encoding="utf8") as backup_file:
        proceeded, stop_byte, link_entries = json.load(backup_file)
    return proceeded, stop_byte


def proceed_text(text):
    for sym in text:
        process_symbol(sym)


def calc_eta(proceeded, skipped=0):
    global START_TIME
    cur_t = time()
    secs = (((6500000 - skipped) / (proceeded - skipped)) * (cur_t - START_TIME)
            + START_TIME - cur_t)
    hours = secs // (60 * 60)
    secs -= hours * 60 * 60
    mins = secs // 60
    secs -= mins * 60
    return " ".join([str(int(hours)), "hours,", str(int(mins)), "minutes,",
                    str(round(secs)), "seconds"])


proceeded_titles = set()


def proceed_file(path, skipped=0, start_byte=0):
    global proceeded_titles, BACKUP_FREQ, NO_BACKUP, RESUME
    global ETA_FREQ
    cur_proceeded = skipped
    reading_text = False
    cur_text_arr = []
    with open(path, "rb") as dump_file:
        dump_file.seek(start_byte)
        for line in dump_file:
            new_str = line
            new_str_stripped = new_str.strip()
            if reading_text:
                str_decoded = str(new_str_stripped, encoding="utf8")
                if new_str_stripped.endswith(b"</text>"):
                    cur_text_arr.append(str_decoded[:-7])
                    cur_text = "".join(cur_text_arr)
                    proceed_text(cur_text)
                    if DEV:
                        print("Proceeded text")
                    cur_proceeded += 1
                    if (not NO_BACKUP and cur_proceeded % BACKUP_FREQ == 0):
                        backup(cur_proceeded, dump_file.tell())
                    if cur_proceeded % ETA_FREQ == 0:
                        print("Proceeded", cur_proceeded, "| ETA:",
                              calc_eta(cur_proceeded, skipped))
                    reading_text = False
                    cur_text_arr = []
                else:
                    cur_text_arr.append(str_decoded)
            else:
                if new_str_stripped.startswith(b"<title>"):
                    title_arr = []
                    str_decoded = str(new_str_stripped, encoding="utf8")
                    i =  7
                    state = 0
                    while not state == 8:
                        if state == 0 and str_decoded[i] == "<":
                            state = 1
                        if state == 1 and str_decoded[i] == "/":
                            state = 2
                        if state == 2 and str_decoded[i] == "t":
                            state = 3
                        if state == 3 and str_decoded[i] == "i":
                            state = 4
                        if state == 4 and str_decoded[i] == "t":
                            state = 5
                        if state == 5 and str_decoded[i] == "l":
                            state = 6
                        if state == 6 and str_decoded[i] == "e":
                            state = 7
                        if state == 7 and str_decoded[i] == ">":
                            state = 8
                        title_arr.append(str_decoded[i])
                        i += 1
                    cur_title = "".join(title_arr[:-8])
                    if DEV:
                        print(cur_title)
                elif new_str_stripped.startswith(b"<text "):
                    if cur_title not in proceeded_titles:
                        proceeded_titles.add(cur_title)
                        text_start_pos = 33
                        str_decoded = str(new_str_stripped, encoding="utf8")
                        for c in str_decoded[33:]:
                            if c == ">":
                                text_start_pos += 1
                                break
                            text_start_pos += 1
                        cur_text_arr.append(str_decoded[text_start_pos:])
                        reading_text = True
                    elif DEV:
                        print("Skip: copy")
        if not NO_BACKUP:
            backup(cur_proceeded, dump_file.tell())


def filter_articles():
    global link_entries, MIN_ENTRIES, MAX_ENTRIES
    res = []
    for article, entries in link_entries.items():
        if MIN_ENTRIES <= entries and entries <= MAX_ENTRIES:
            res.append([entries, article])
    res.sort(key=lambda entry: [-entry[0], entry[1]])
    return res


def get_link_href(link):
    res = "https://ru.wikipedia.org/wiki/" + escape(
        link.translate({ord(" "): "_"}))
    return res


def generate_html_pages(limit):
    global HTML_OUTPUT, HTML_TEMPLATE, DEV
    articles = filter_articles()
    if DEV:
        print("Articles:")
        print(articles)
    pos = 0
    total_pages = (len(articles) - 1) // limit + 1
    j2_env = Environment(loader=FileSystemLoader("./", encoding="utf-8"))
    page_template = j2_env.get_template(HTML_TEMPLATE)
    if os.path.exists(HTML_OUTPUT):
        rmtree(HTML_OUTPUT)
    os.mkdir(HTML_OUTPUT)
    for i in range(1, total_pages + 1):
        if DEV:
            print("Page " + str(i))
        cur_articles = dict()
        for _ in range(limit):
            if pos >= len(articles):
                break
            cur_articles[articles[pos][1]] = (
                pos + 1, get_link_href(articles[pos][1]), articles[pos][0]
            )
            pos += 1
        page_file = HTML_OUTPUT + "/page" + str(i) + ".html"
        with open(page_file, "w", encoding="utf-8") as output:
            prev_page_exists = i != 1
            prev_page = "page" + str(i - 1) + ".html"
            next_page_exists = i != total_pages
            next_page = "page" + str(i + 1) + ".html"
            output.write(page_template.render(
                page=i, total=total_pages,
                min_entries=MIN_ENTRIES, max_entries=MAX_ENTRIES,
                prev_page_exists=prev_page_exists, prev_page=prev_page,
                next_page_exists=next_page_exists, next_page=next_page,
                articles=cur_articles))


START_TIME = time()
unpack_args()
already_proceeded = 0
start_byte = 0
if RESUME:
    already_proceeded, start_byte = unpack_backup()
proceed_file(WIKI_FILE, already_proceeded, start_byte)
print("Proceeded entire dump in", time() - START_TIME, "seconds.")
print("Starting HTML generator...")
generate_html_pages(PAGE_LIMIT)
print("Complete")
webbrowser.open("file://" + os.path.realpath(HTML_OUTPUT + "/page1.html"))
