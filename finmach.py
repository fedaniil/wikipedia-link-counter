# -*- coding: utf-8 -*-
class FiniteStateMachine:
	state = 1
	event_subscribers = dict()

	def __init__(self, switch_matrix, special_states, handlers):
		self.switch = switch_matrix
		self.special_states = special_states
		self.handlers = handlers

	def on(self, event, handler):
		if event not in event_subscribers:
			event_subscribers[event] = []
		event_subscribers[event].append(handler)

	def __dispatch(self, event, payload):
		for subscriber in event_subscribers[event]:
			subscriber(payload)