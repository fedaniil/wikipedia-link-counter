# wikipedia-link-counter
Вы можете посмотреть это описание [на русском]

Status: about fully-functional, but code still may be a bit messy.

Few words about versions: v1.0 is the one active on 23:59 9.05 Moscow time and v1.1 is the one active on 23:59 10.05 Moscow time, I'm not sure which one is a deadline for the task. v1.1 has some significant improvments: some optimiztions, ETA logging and possibility to use backup file to reduce computation time over multiple queries, so it is for sure preferred one.
After 23:59 10 May Moscow time no commits related to code were made, only commits related to russian verison of README.

## Install
1. Clone the repository, e.g.

`git clone git@github.com:fedaniil/wikipedia-link-counter`

2. Make sure you have Python 3 and pip installed

3. Install required package Jinja2 with pip

`pip install Jinja2`

## Usage
Default case:

1. Place `ruwiki-latest-pages-articles-multistream.xml` file in repository

2. `python main.py`

You may want to disable backup of dictionary with all article titles and number of their entries in JSON format via passing `--noback` parameter, BUT backup will allow you to proceed Wiki dump once and then generate HTML pages with any parameters you want in a few seconds. Just add `-r` option to skip articles that were accounted in last backup. Also this allows to continue running after unexpected stop/error.

`python main.py -r`

There are some changeable parameters including backup frequency, to view the list and descriptions, use help command:

`python main.py -h`

By default, generated HTML-pages will be located at /html subdirectory (stored in HTML_OUTPUT constant). There are links to previous and next pages included on every page.

Python should probably open the first page in your default browser as soon as all pages will be generated. If that didn't happen, open HTML_OUTPUT/page1.html manually.

Note that if you're re-running program, you need to do one of the following:
- run the program with admin permissions
- remove HTML_OUTPUT subdirectory manually
- choose different HTML_OUTPUT directory via `--htmldir` parameter.

## Test
I've included two files for testing: `wikitest_tiny.xml` with 1 very simple article inside and `wikitest_big.xml` with 2 big articles inside. Pass them to program via `-f` or `--file` param and don't forget to set appropriate `--min` and `--max` params.

## TODO notes
That section reminds me of things I haven't done yet.
1. ~~Take constants from script parameters~~
2. ~~Adjust default constant values for processing big file~~
3. ~~Add second file to process (updates file) before the main dump~~ *(cancelled because I couldn't find "updates" Wikipedia file now)*
4. Beautify code: separate it on several little modules
5. ~~Code for resuming work after unexpected errors using backup~~ *(yay!)*

If still some time left, further improvements:

6. Some styling
7. Run a webserver (Python/Node?)
8. Migrate to client-generated page method (Svelte?)
9. Run the server on fda.pp.ua (that's why I want to use Node on step 7)

[на русском]: /README_RU.md "README on Russian"
